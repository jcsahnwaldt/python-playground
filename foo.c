
int foo(int x) {
  return x + 1;
}

int bar(int y) {
  return y - 1;
}

int callback(int fun(int), int arg) {
  return fun(arg);
}
