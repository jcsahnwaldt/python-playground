libfoo.dylib: foo.c
libfoo.so: foo.c

lib%.dylib: %.c
	$(CC) -dynamiclib -o $@ $<

lib%.so: %.c
	$(CC) -shared -o $@ -fPIC $<

clean:
	$(RM) *.dylib *.so
