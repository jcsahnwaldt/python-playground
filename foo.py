from ctypes import *
from ctypes.util import *

libfoo = CDLL(find_library('libfoo'))

@CFUNCTYPE(c_int, c_int)
def foo(x):
    return x + 1

@CFUNCTYPE(c_int, c_int)
def bar(x):
    return x - 1

print(libfoo.foo(1))
print(libfoo.bar(1))

print(libfoo.callback(foo, 1))
print(libfoo.callback(bar, 1))
